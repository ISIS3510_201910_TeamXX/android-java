package com.ciclap.ciclap.Adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciclap.ciclap.Entidades.misRutas;
import com.ciclap.ciclap.R;

import java.util.ArrayList;

public class AdapterMisRutas extends RecyclerView.Adapter<AdapterMisRutas.ViewHolder> {


    ArrayList<misRutas> misRutasArrayList;

    public AdapterMisRutas(ArrayList<misRutas> misRutas ){

        this.misRutasArrayList=misRutas;

    }


    @NonNull
    @Override
    public AdapterMisRutas.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_mis_rutas,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMisRutas.ViewHolder viewHolder, int i) {

        viewHolder.nombreRuta.setText(misRutasArrayList.get(i).getNombre());
        viewHolder.descripcionRuta.setText(misRutasArrayList.get(i).getDescripcion());
        viewHolder.direccionRuta.setText(misRutasArrayList.get(i).getDestino());

    }

    @Override
    public int getItemCount() {
        return misRutasArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nombreRuta;
        TextView descripcionRuta;
        TextView direccionRuta;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nombreRuta= itemView.findViewById(R.id.nombreRuta);
            descripcionRuta= itemView.findViewById(R.id.descripcionRuta);
            direccionRuta= itemView.findViewById(R.id.direccionRuta);
        }
    }
}
