package com.ciclap.ciclap.Adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciclap.ciclap.Fragments.FragmentHistorial.OnListFragmentInteractionListener;
import com.ciclap.ciclap.R;
import com.ciclap.ciclap.Entidades.Historial;

import java.util.ArrayList;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<Historial> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyItemRecyclerViewAdapter(ArrayList<Historial> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);

        //view.setOnClickListener(mOnClickListener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.origen.setText(mValues.get(position).getOrigen());
        holder.destino.setText(mValues.get(position).getDestino());
        holder.fecha.setText(mValues.get(position).getFecha());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem,position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView origen;
        public final TextView destino;
        public final TextView fecha;
        public Historial mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            origen = (TextView) view.findViewById(R.id.bajada);
            destino = (TextView) view.findViewById(R.id.subida);
            fecha = (TextView) view.findViewById(R.id.fecha);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + destino.getText() + "'";
        }
    }

}
