package com.ciclap.ciclap.Adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciclap.ciclap.Entidades.Recientes;
import com.ciclap.ciclap.R;

import java.util.ArrayList;

public class AdapterRecientes extends RecyclerView.Adapter<AdapterRecientes.ViewHolder> {


    ArrayList<Recientes> recientesArray;

    public AdapterRecientes( ArrayList<Recientes> recientes ){

        this.recientesArray=recientes;

    }


    @NonNull
    @Override
    public AdapterRecientes.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_recientes,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecientes.ViewHolder viewHolder, int i) {

        viewHolder.nombreReciente.setText(recientesArray.get(i).getNombreRecientes());
        viewHolder.direccionReciente.setText(recientesArray.get(i).getDireccionesRecciones());

    }

    @Override
    public int getItemCount() {
        return recientesArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nombreReciente;
        TextView direccionReciente;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nombreReciente= itemView.findViewById(R.id.recientesNombre);
            direccionReciente= itemView.findViewById(R.id.recientesDireccion);
        }
    }
}
