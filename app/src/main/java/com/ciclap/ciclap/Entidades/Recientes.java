package com.ciclap.ciclap.Entidades;

public class Recientes {

    private String nombreRecientes;
    private String direccionesRecciones;

    public Recientes(String nombreRecientes, String direccionesRecciones) {
        this.nombreRecientes = nombreRecientes;
        this.direccionesRecciones = direccionesRecciones;
    }

    public String getNombreRecientes() {
        return nombreRecientes;
    }

    public void setNombreRecientes(String nombreRecientes) {
        this.nombreRecientes = nombreRecientes;
    }

    public String getDireccionesRecciones() {
        return direccionesRecciones;
    }

    public void setDireccionesRecciones(String direccionesRecciones) {
        this.direccionesRecciones = direccionesRecciones;
    }
}
