package com.ciclap.ciclap.Entidades;
import java.io.Serializable;

public class Historial implements Serializable{
    private String origen;
    private String destino;
    private String fecha;

    public Historial(String origen, String destino, String fecha) {
        this.origen = origen;
        this.destino = destino;
        this.fecha = fecha;
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }

    public String getFecha() {
        return fecha;
    }
}
