package com.ciclap.ciclap.Entidades;

public class misRutas {

private String nombre;
private String origen;
private String destino;
private String descripcion;

    public misRutas(String nombre, String origen, String destino, String descripcion) {
        this.nombre = nombre;
        this.origen = origen;
        this.destino = destino;
        this.descripcion = descripcion;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
