package com.ciclap.ciclap.Entidades;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class Ruta {


    private static final String TAG = "RUTA";
    private ArrayList<LatLng> coordenadas;

    public Ruta(ArrayList<String> lista){
        setCoordenadas(lista);
    }

    public ArrayList getCoordenadas(){
        return coordenadas;
    }


    public void setCoordenadas(ArrayList<String> lista){

        ArrayList<LatLng> coords = new ArrayList<>();
        for (int i = 0; i <lista.size() ; i++) {

            String temp = lista.get(i);

            StringBuilder builder = new StringBuilder(temp);
            builder.deleteCharAt(temp.length()-1);
            builder.deleteCharAt(0);

            temp = builder.toString();

            String[] arr = temp.split(",");

            LatLng nuevo = new LatLng(Double.parseDouble(arr[1]),Double.parseDouble(arr[0]));
            Log.d(TAG,"el mansito "+ i + " tiene "+  nuevo.toString());

            coords.add(nuevo);
        }
        coordenadas = coords;
    }





}
