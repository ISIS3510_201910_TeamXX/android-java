package com.ciclap.ciclap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {


    private static final String CERO = "0";
    private static final String BARRA = "/";

    // Int
    private int dia, mes, anio;

    //## Attributes - Edit Text
    private EditText correE;
    private EditText nombre;
    private EditText peso;
    private EditText altura;
    private EditText edad;
    private EditText contrasenia;

    //## Attributes - Edit Text
    private String correoElectronico = "";
    private String nombreUsario = "";
    private String pesoUsuario = "";
    private String alturaUsuario = "";
    private String edadUsuario = "";
    private String contraseniaUsuario;

    private CardView registerCardView;

    //## Firebase Auth
    FirebaseAuth firebase;
    DatabaseReference dataBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        correE = findViewById(R.id.registerCorreo);
        nombre = findViewById(R.id.registerNombre);
        peso = findViewById(R.id.registerWeight);
        altura = findViewById(R.id.registerHeight);
        edad = findViewById(R.id.registerAge);
        contrasenia = findViewById(R.id.registerPassword);

        firebase = FirebaseAuth.getInstance();
        dataBase = FirebaseDatabase.getInstance().getReference();

        edad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFecha();
            }
        });

        registerCardView = findViewById(R.id.cardRegister);
        registerCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                correoElectronico = correE.getText().toString();
                nombreUsario = nombre.getText().toString();
                pesoUsuario = peso.getText().toString();
                alturaUsuario = altura.getText().toString();
                edadUsuario = edad.getText().toString();
                contraseniaUsuario = contrasenia.getText().toString();

                if (!correoElectronico.isEmpty() && !nombreUsario.isEmpty() && !pesoUsuario.isEmpty() &&
                        !alturaUsuario.isEmpty() && !edadUsuario.isEmpty()  && !contraseniaUsuario.isEmpty()){

                    if (isEmailValid(correoElectronico)){
                        if(contraseniaUsuario.length() >= 6){
                            registerUser();
                        }

                        else {
                            Toast.makeText(RegisterActivity.this, "Su contraseña debe tener por lo menos 6 caracteres", Toast.LENGTH_SHORT).show();
                        }
                    }

                    else{
                        Toast.makeText(RegisterActivity.this, "El correo ingresado no es válido", Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(RegisterActivity.this, "Ninguno de los campos debe ser vacío", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------------------------------

    private void obtenerFecha(){

        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                edad.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);

            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        },anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();
    }



    private void registerUser(){
        firebase.createUserWithEmailAndPassword(correoElectronico, contraseniaUsuario).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> taskA) {
                if(taskA.isSuccessful()){

                    Map <String, Object> map = new HashMap<>();
                    map.put("calidad_aire", "0");
                    map.put("calorias", "0");
                    map.put("kilometros", "0");
                    map.put("nombre", nombreUsario);

                    String userID = firebase.getCurrentUser().getUid();
                    dataBase.child("usuarios").child(userID).child("user").setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> taskTwo) {
                            if(taskTwo.isSuccessful()){
                                startActivity(new Intent(RegisterActivity.this, ChangeProfilePhotoFirstActivity.class));
                                finish();
                            }

                            else {
                                Toast.makeText(RegisterActivity.this, "No se pudieron cargar sus datos",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(RegisterActivity.this, "El registro no fue exitoso", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    /// Validar E-MAIL
    public boolean isEmailValid (String email)
    {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }
}
