package com.ciclap.ciclap;

import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ciclap.ciclap.Entidades.Ruta;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class NavigationActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "ola";
    private GoogleMap mMap;
    private Address inicio;
    private LatLng destino;
    private LatLng ubicacion;
    private Ruta ruta;
    private String cicloRuta;
    private String tiempo;
    private String distancia;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_navigation);
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.navigationLayout);
//        mapFragment.getMapAsync(this);
//
//        Bundle bundle = getIntent().getExtras();
//        if(bundle != null){
//            if(bundle.getString("llego al bundle")!=null){
//                Toast.makeText(getApplicationContext(),"data:" + bundle.getString("inicio"), Toast.LENGTH_SHORT).show();
//            }
//        }
//
////        FragmentManager manager = getSupportFragmentManager();
////        FragmentTransaction t = manager.beginTransaction();
////        FragmentRutas fragmentRutas = new FragmentRutas();
////        t.add(R.id.navigationLayout, fragmentRutas);
////        t.commit();
//
//    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigationLayout);
        mapFragment.getMapAsync(this);
        Intent intent = getIntent();
        if(intent.getStringExtra("inicio")!= null)
        {
            LatLng inicio = intent.getExtras().getParcelable("inicio");
            ubicacion = inicio;
        }

        else {
            ubicacion = intent.getParcelableExtra("ubicacion");

        }




        cicloRuta = intent.getStringExtra("cicloRuta");

        LatLng autoDestino = intent.getExtras().getParcelable("destino");
        destino = autoDestino;

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "";
        if(cicloRuta.equals("true")){
            url ="https://api.openrouteservice.org/v2/directions/cycling-regular?api_key=5b3ce3597851110001cf6248269309a778d64ba4b58eb255bc79ddee&start="+
                    ubicacion.longitude +","+ ubicacion.latitude+ "&end=" + autoDestino.longitude +","+ autoDestino.latitude;
        }
        else{
            url ="https://api.openrouteservice.org/v2/directions/cycling-road?api_key=5b3ce3597851110001cf6248269309a778d64ba4b58eb255bc79ddee&start="+
                    ubicacion.longitude +","+ ubicacion.latitude+ "&end=" + autoDestino.longitude +","+ autoDestino.latitude;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d(TAG, "Response is: "+ response.substring(0,500));
                        JSONObject res = new JSONObject();

                        try {
                            res = new JSONObject(response);
                            JSONArray features;

                            features = res.getJSONArray("features");
                            Log.d(TAG, "features es "+ features);
                            JSONObject algo;
                            algo = features.getJSONObject(0);
                            Log.d(TAG, "algo  es "+ algo);

                            JSONObject geom = algo.getJSONObject("geometry");
                            JSONObject props = algo.getJSONObject("properties");
                            JSONObject sum = props.getJSONObject("summary");
                            distancia = sum.get("distance").toString();
                            tiempo = Double.toString((double) (sum.get("duration"))/60);
                            Log.d(TAG, "distancia es "+ distancia);
                            Log.d(TAG, "tiempo es "+ tiempo);
                            TextView dist = (TextView) findViewById(R.id.distanciap);
                            dist.setText(distancia + " metros");
                            TextView tiempov = (TextView) findViewById(R.id.tiempop);
                            tiempov.setText(tiempo+ " minutos") ;



                            Log.d(TAG, "geometry es "+ geom);
                            JSONArray coords;
                            coords = geom.getJSONArray("coordinates");


                            ArrayList listaCoords = new ArrayList();
                            for(int i=0; i < coords.length();i ++)
                            {
                                listaCoords.add(coords.getString(i));

                            }
                            Log.d(TAG, "coords es "+ listaCoords);
                            ruta = new Ruta(listaCoords);
                            if(ruta != null){
                                Log.d(TAG, "la ruta se creo");
                            }
                            else{
                                Log.d(TAG, "la ruta NO se creo");
                            }
                            pintaRuta();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        pintaRuta();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "NO SIRVE");
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

        //Toast.makeText(getApplicationContext(),inicio, Toast.LENGTH_SHORT).show();


    }
    public void pintaRuta(){

        PolylineOptions options = new PolylineOptions();
        options.width(5);
        options.color(Color.MAGENTA);
        ArrayList<LatLng> cords = ruta.getCoordenadas();
        Log.d(TAG, " bicho en el 0 " + cords.get(0).toString());
        Log.d(TAG, " bicho en el 10 " + cords.get(10).toString());

        LatLng ini = cords.get(0);
        LatLng fin = cords.get(cords.size()-1);
//        options.add(new LatLng(ini.longitude,ini.latitude));
//        options.add(new LatLng(fin.longitude,fin.latitude));

        Log.d(TAG, " va a pintar la ruta");
        Log.d(TAG, " destino lat = " + destino.latitude);
        options.addAll(cords);
        //options.add(ubicacion);
        //options.add(new LatLng(destino.getLatitude(), destino.getLongitude()));

        mMap.addPolyline(options);
    }

    public void geolocateInicio(String pinicio){


        Log.d(TAG, "GEOLOCATING");
        String searchStr = pinicio;


        Geocoder geocoder = new Geocoder(this);
        //ArrayList<Address> lista = new ArrayList<>();
        List<Address> lista = new ArrayList<>();
        try{
            lista = geocoder.getFromLocationName(searchStr,1);

        }catch (IOException e){
            Log.e(TAG, "Geolocate exception" + e.getMessage());
        }
        if(lista.size() > 0)
        {
            Address lugarInicio = lista.get(0);
            inicio = lugarInicio;
            //Toast.makeText(this,lugarInicio.toString(), Toast.LENGTH_SHORT ).show();

        }
    }
//    public void geolocateDestino(String pdestino){
//
//
//        Log.d(TAG, "GEOLOCATING");
//        String searchStr = pdestino;
//        Geocoder geocoder = new Geocoder(this);
//        //ArrayList<Address> lista = new ArrayList<>();
//        List<Address> lista = new ArrayList<>();
//        try{
//            lista = geocoder.getFromLocationName(searchStr,1);
//            Log.d(TAG,lista.get(0).toString());
//
//
//
//        }catch (IOException e){
//            Log.e(TAG, "Geolocate exception " + e.getMessage());
//        }
//        if(lista.size() > 0)
//        {
//
//            Address lugarDestino = lista.get(0);
//            Log.d(TAG,lugarDestino.toString());
//            destino = lugarDestino;
//           // Toast.makeText(this,lugarDestino.toString(), Toast.LENGTH_SHORT ).show();
//        }
//
//    }




    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng ini = new LatLng(0,0);
        if(ubicacion != null)
        {
             ini = ubicacion;

        }
        else
        {
             ini = new LatLng(inicio.getLatitude(), inicio.getLongitude());

        }


        // Add a marker in Sydney, Australia, and move the camera.
        mMap.addMarker(new MarkerOptions().position(ini).title("Marker in inicio"));
        mMap.addMarker(new MarkerOptions().position(destino).title("Marker in destino"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(ini));
        float zoomLevel = (float) 14.5;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ini, zoomLevel));
        mMap.setMyLocationEnabled(true);

        //mMap.animateCamera(CameraUpdateFactory.zoomTo(mMap.getCameraPosition().zoom - 0.5f));
    }
}
