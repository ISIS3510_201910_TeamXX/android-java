package com.ciclap.ciclap.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciclap.ciclap.Adapters.AdapterMisRutas;
import com.ciclap.ciclap.Entidades.misRutas;
import com.ciclap.ciclap.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class FragmentPrincipal extends Fragment {

    protected FragmentActivity mActivity;

    private ImageView textViewEmpty;
    private RecyclerView recyclerView;
    private ArrayList<misRutas> misRutasArrayList;
    private AdapterMisRutas adapterMisRutas;

    //Base de datos
    private DatabaseReference databaseReference;
    private OnFragmentInteractionListener mListener;
    private TextView rutas;


    public FragmentPrincipal() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.fragment_principal, container, false);


        TextView textoAniadir = view.findViewById(R.id.aniadirRuta);
        textoAniadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),FragmentAgregarRuta.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        misRutasArrayList= new ArrayList<>();
        rutas=view.findViewById(R.id.ruta);
        rutas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isConnected()) buildDialog().show();
                else {
                    Fragment fragment = new FragmentRutas();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.principal,fragment).commit();
                }
            }
        });


        recyclerView = (RecyclerView) view.findViewById(R.id.mis_rutas);
        recyclerView.setLayoutManager( new LinearLayoutManager(getContext()));
        datos();

        //textViewEmpty = view.findViewById(R.id.empty_view);
//        Activity activityInner = getActivity();
//        activityInner.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//                if (misRutasArrayList.isEmpty()){
//                    recyclerView.setVisibility(view.GONE);
//                    textViewEmpty.setVisibility(view.VISIBLE);
//                }
//
//                else {
//                    textViewEmpty.setVisibility(view.GONE);
//                    recyclerView.setVisibility(view.VISIBLE);
//                }
//            }
//        });
        

        return view;
    }


    public void datos(){

        // Intento para SharedPreferences - Lista
        int contador = 0;

        if (isConnected()){

            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
            databaseReference = FirebaseDatabase.getInstance().getReference("usuarios").child(currentUser.getUid());

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if(dataSnapshot.exists()){

                        misRutasArrayList= new ArrayList<>();

                        for(DataSnapshot ds: dataSnapshot.getChildren()){

                            Log.i("Si si ", ds.child("mis_rutas").getChildren().toString());
                            for (DataSnapshot d: ds.child("mis_rutas").getChildren()){

                                String nombre= d.child("nombre").getValue().toString();
                                String origen= d.child("origen").getValue().toString();
                                String destino= d.child("destino").getValue().toString();
                                String descripcion= d.child("descripcion").getValue().toString();

                                misRutasArrayList.add(new misRutas(nombre,origen,destino,descripcion));


                                // Create SharedPreferences
//                                Context ct = getContext();
//                                SharedPreferences preferencias = getContext().getSharedPreferences("Mis_Rutas", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = preferencias.edit();
//
//                                // Cosas Extras para el JSON
//                                Gson gson = new Gson();
//                                String json = gson.toJson(misRutasArrayList);
//                                editor.putString("Lista_Mis_Rutas", json);
//
//                                editor.apply();
                            }
                        }
                        Log.d("tamaño", misRutasArrayList.size()+"");
                        adapterMisRutas = new AdapterMisRutas(misRutasArrayList);
                        recyclerView.setAdapter(adapterMisRutas);


                        final GestureDetector mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
                            @Override public boolean onSingleTapUp(MotionEvent e) {
                                return true;
                            }
                        });

                        //
                        // ACAAAAAAAAAAAAA
                        // ESTA
                        //

                        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                            @Override
                            public void onRequestDisallowInterceptTouchEvent(boolean b) {

                            }

                            @Override
                            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                                try {
                                    View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                                    if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {

                                        int position = recyclerView.getChildAdapterPosition(child);

                                        Toast.makeText(getContext(),"The Item Clicked is: "+ position ,Toast.LENGTH_SHORT).show();

                                        return true;
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                                return false;
                            }

                            @Override
                            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

                            }
                        });

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else {

            //TODO: Corregir para que sirva con todas las rutas

            Context ct = getActivity();
            SharedPreferences preferencias = ct.getSharedPreferences("Mis_Rutas", Context.MODE_PRIVATE);
            Gson gson = new Gson();
            String json = preferencias.getString("Lista_Mis_Rutas", "No hay información guardada");

            Type type = new TypeToken<ArrayList<misRutas>>(){}.getType();
            misRutasArrayList = gson.fromJson(json, type);

            Log.d("tamaño", misRutasArrayList.size()+"");
            adapterMisRutas = new AdapterMisRutas(misRutasArrayList);
            recyclerView.setAdapter(adapterMisRutas);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }



    // Check if is Connected
    public boolean isConnected() {

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
        else return false;
        } else
        return false;
    }


    // Dialogo de Sin Conexion

    public AlertDialog.Builder buildDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("No hay conexión a Internet");
        builder.setMessage("Necesitas estar conectado para acceder a esta función");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
               dialog.dismiss();
            }
        });

        return builder;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            mActivity = (FragmentActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
