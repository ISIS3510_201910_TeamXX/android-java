package com.ciclap.ciclap.Fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ciclap.ciclap.Adapters.MyItemRecyclerViewAdapter;
import com.ciclap.ciclap.R;
import com.ciclap.ciclap.Entidades.Historial;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FragmentHistorial extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    private ArrayList<Historial> listaHistorial;
    private OnListFragmentInteractionListener mListener;
    private DatabaseReference databaseReference;

    RecyclerView recyclerView;

    public FragmentHistorial() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static FragmentHistorial newInstance(int columnCount) {
        FragmentHistorial fragment = new FragmentHistorial();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listaHistorial = new ArrayList<>();

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        llenar();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;

        }
        return view;
    }

    public void llenar() {


        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("usuarios").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    listaHistorial = new ArrayList<>();
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {

                        for (DataSnapshot d : ds.child("historial_rutas").getChildren()) {

                            String destino = d.child("destino").getValue().toString();
                            String fecha = d.child("fecha").getValue().toString();
                            String origen = d.child("origen").getValue().toString();


                            listaHistorial.add(new Historial(origen, destino, fecha));
                        }
                    }
                    if (mColumnCount <= 1) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    } else {
                        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), mColumnCount));
                    }
                    recyclerView.setAdapter(new MyItemRecyclerViewAdapter(listaHistorial, mListener));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        for (int i = 0; i < 10; i++) {
            Historial historial = new Historial("CLL 34 #20-1" + i, "CR 20 #23-4" + i, "10/15/20");
            listaHistorial.add(historial);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Historial item, int pos);
    }
}
