package com.ciclap.ciclap.Fragments;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.ciclap.ciclap.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.isapanah.awesomespinner.AwesomeSpinner;

public class FragmentAgregarPuntosInteres extends FragmentActivity implements OnMapReadyCallback {


    //--- Google Map
    private GoogleMap mMap;

    private AwesomeSpinner opcionPuntoInteres;

    //-- Text form Spinner
    private String spinnerText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_fragment_comunidad_agrega);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapAgregarPuntosInteres);
        mapFragment.getMapAsync(FragmentAgregarPuntosInteres.this);

        opcionPuntoInteres = findViewById(R.id.opcionesPtoInteres);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.comboTipoPtoInteres, android.R.layout.simple_spinner_item);
        opcionPuntoInteres.setAdapter(adapter,0);

        opcionPuntoInteres.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                spinnerText = itemAtPosition;
                Toast.makeText(getApplicationContext(), "Seleccionado: " + spinnerText, Toast.LENGTH_SHORT).show();
            }
        });

    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

}
