package com.ciclap.ciclap.Fragments;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ciclap.ciclap.ChangeProfilePhotoFirstActivity;
import com.ciclap.ciclap.Home;
import com.ciclap.ciclap.R;
import com.ciclap.ciclap.RegisterActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FragmentSendReport extends AppCompatActivity {

    // Atributos de FireBase :D
    FirebaseAuth firebase;
    DatabaseReference dataBase;
    FirebaseUser usuarioActual;

    // Comentario
    EditText textoReclamo;
    String reclamo;

    //CardView
    CardView boton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_send_report);

        firebase = FirebaseAuth.getInstance();
        dataBase = FirebaseDatabase.getInstance().getReference();
        usuarioActual = FirebaseAuth.getInstance().getCurrentUser();

        textoReclamo = findViewById(R.id.textReclamo);
        reclamo = textoReclamo.getText().toString();
        boton = (CardView) findViewById(R.id.cardEnviarReporteBotoncito);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarComentario();
            }
        });

    }

    public void registrarComentario(){
        reclamo = textoReclamo.getText().toString();
        Date currentT = Calendar.getInstance().getTime();
        String time = currentT.toString();

        Map<String, Object> map = new HashMap<>();
        map.put(time + "__" + usuarioActual.getUid(),reclamo);
        dataBase.child("problemas").setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> taskTwo) {
                if(taskTwo.isSuccessful()){
                    Toast.makeText(FragmentSendReport.this, "Su problema se ha cargado exitosamente, nuestro equipo lo evaluará pronto.",
                            Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(FragmentSendReport.this, Home.class));
                }

                else {
                    Toast.makeText(FragmentSendReport.this, "No se pudo cargar tu problema",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
