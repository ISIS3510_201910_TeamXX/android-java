package com.ciclap.ciclap.Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ciclap.ciclap.Home;
import com.ciclap.ciclap.R;

public class RutaCreadaActivity extends AppCompatActivity {

    CardView card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruta_creada);

        card = findViewById(R.id.cardRutaCreada);
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RutaCreadaActivity.this, Home.class);
                startActivity(intent);

            }
        });
    }
}
