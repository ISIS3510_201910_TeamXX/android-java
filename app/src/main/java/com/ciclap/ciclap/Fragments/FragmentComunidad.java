package com.ciclap.ciclap.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.ciclap.ciclap.R;

public class FragmentComunidad extends Fragment {


    // Attributes - Card Views
    private CardView informa, califica;
    private Button agrega, explora;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_comunidad,container,false);

        informa = view.findViewById(R.id.cardInforma);
        califica = view.findViewById(R.id.cardCalifica);

        agrega = view.findViewById(R.id.buttonAgregarAyuda);
        explora = view.findViewById(R.id.buttonExplorarAyuda);


        // Activities from Add Option
        agrega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getContext(), FragmentAgregarPuntosInteres.class);
                startActivity(intent);
            }
        });


        // Activities form Explore Option
        explora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), FragmentPuntosInteres.class);
                startActivity(intent);
            }
        });


        // Activities for Stars
        califica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        informa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), FragmentInformarFacebook.class));
                getActivity().finish();
            }
        });


        return view;
    }

}
