package com.ciclap.ciclap.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciclap.ciclap.Adapters.AdapterRecientes;
import com.ciclap.ciclap.Entidades.Recientes;
import com.ciclap.ciclap.NavigationActivity;
import com.ciclap.ciclap.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentRutas.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentRutas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRutas extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "initializing";

    private static final int AUTOCOMPLETE_INICIO = 1;
    private static final int AUTOCOMPLETE_DESTINO = 2;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private ArrayList<Recientes> recientesArrayList;
    private AdapterRecientes adapterRecientes;

    private PlacesClient placesClient;
    private FragmentActivity myContext;

    private TextView inputInicio;
    private TextView inputDestino;
    private Button okButton;



    private LatLng ubicacionActual;
    private LatLng autoDestino;

    private FusedLocationProviderClient client;

    private boolean cicloRuta;

    private OnFragmentInteractionListener mListener;

    public FragmentRutas() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentRutas.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRutas newInstance(String param1, String param2) {
        FragmentRutas fragment = new FragmentRutas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
        if (!Places.isInitialized()){
            Places.initialize(getThisContext(), "AIzaSyCOQT4gfDJWMxg-XYRk413ROMF4I3TsZzE");

        }
        placesClient = Places.createClient(getThisContext());

//        final AutocompleteSupportFragment autoComplete = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
//        autoComplete.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME));
//
//        // Set up a PlaceSelectionListener to handle the response.
//        autoComplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//            @Override
//            public void onPlaceSelected(Place place) {
//                // TODO: Get info about the selected place.
//                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
//            }
//
//            @Override
//            public void onError(Status status) {
//                // TODO: Handle the error.
//                Log.i(TAG, "An error occurred: " + status);
//            }
//        });






    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_DESTINO) {
            if (resultCode == RESULT_OK) {


                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                autoDestino = place.getLatLng();
                Log.i(TAG, autoDestino.toString());
                inputDestino = (TextView) getView().findViewById(R.id.inputDestino);
                inputDestino.setText(place.getName());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == AUTOCOMPLETE_INICIO) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                ubicacionActual = place.getLatLng();
                Log.i(TAG, ubicacionActual.toString());
                inputInicio = (TextView) getView().findViewById(R.id.inputinicio);
                inputInicio.setText(place.getName());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
    public Context getThisContext()
    {
        Context context = getActivity().getApplicationContext(); //saca contexto

        return context;

    }

    public void checkUbicacion()
    {
        Context context = getActivity().getApplicationContext(); //saca contexto


        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);

        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            Toast.makeText(context,"Porfavor prenda la ubicacion", Toast.LENGTH_SHORT ).show();


        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        checkUbicacion();
        iniciarPlaces();
        Context context = getActivity().getApplicationContext(); //saca contexto

        View view = inflater.inflate(R.layout.fragment_fragment_rutas, container, false);
        recientesArrayList = new ArrayList<>();

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerRecientes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        datos();



        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {

        } else {
            Toast.makeText(context,"ERROR DE PERMISO UBICACION", Toast.LENGTH_SHORT ).show();
        }

        //autocomplete();
        adapterRecientes = new AdapterRecientes(recientesArrayList);
        recyclerView.setAdapter(adapterRecientes);
        Button okbutton = (Button) view.findViewById(R.id.okButton);
        inputInicio = (TextView) view.findViewById(R.id.inputinicio);
        inputDestino = (TextView) view.findViewById(R.id.inputDestino);

        inputDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



// Set the fields to specify which types of place data to
// return after the user has made a selection.
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

// Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields).setCountry("COL")
                        .build(getThisContext());

                startActivityForResult(intent, AUTOCOMPLETE_DESTINO);

            }
        });
        inputInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



// Set the fields to specify which types of place data to
// return after the user has made a selection.
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

// Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .build(getThisContext());
                startActivityForResult(intent, AUTOCOMPLETE_INICIO);

            }
        });
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ubicacionActual != null && autoDestino != null){

                    Intent intent = new Intent(getActivity(), NavigationActivity.class);

                    intent.putExtra("ubicacion", ubicacionActual);





                    intent.putExtra("destino", autoDestino);
                    intent.putExtra("cicloRuta", "true");

                    Log.d("myTag", "This is my message");
                    //getActivity().finish();
                    startActivity(intent);
                }
               else{
                    Toast.makeText(getThisContext(),"Por favor ingrese el destino y el punto de partida", Toast.LENGTH_SHORT ).show();

                }



            }
        });
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ImageButton butUbi = (ImageButton) view.findViewById(R.id.buttonUbicacion);
        client = LocationServices.getFusedLocationProviderClient(context);
        butUbi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Context context = getActivity().getApplicationContext();

                try{
                    client.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>(){

                        @Override
                        public void onSuccess(Location location){
                            if(location!= null ){
                                ubicacionActual = new LatLng(location.getLatitude(),location.getLongitude());
                                Toast.makeText(getActivity().getApplicationContext(),"Se guardo su ubicacion actual", Toast.LENGTH_SHORT ).show();
                                TextView inicio = getView().findViewById(R.id.inputinicio);
                                inicio.setText("Ubicación Actual");

                            }
                            checkUbicacion();



                        }
                    });
                }
                catch(Exception e)
                {
                    checkUbicacion();
                }


            }
        });

        Switch coso =view.findViewById(R.id.switch1);
        coso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               cicloRuta = true;
            }
        });


        return view;
    }
    private PlacesClient iniciarPlaces(){

        try{
            Places.initialize(getThisContext(), "AIzaSyCOQT4gfDJWMxg-XYRk413ROMF4I3TsZzE");
            Log.i(TAG,"se inicializo places" );
        }
        catch (Exception e)
        {

        }


// Create a new Places client instance
        PlacesClient placesClient = Places.createClient(getThisContext());
        return placesClient;

    }








    public void datos() {

        recientesArrayList.add(new Recientes("Teatron", "Cra 123 #154-1"));
        recientesArrayList.add(new Recientes("Carulla 123", "Cra 123 #154-1"));
        recientesArrayList.add(new Recientes("Casa", "Cra 123 #154-1"));
        recientesArrayList.add(new Recientes("Presea", "Cra 123 #154-1"));
        recientesArrayList.add(new Recientes("Trabajo", "Cra 123 #154-1"));
        recientesArrayList.add(new Recientes("Doña blanca", "Cra 123 #154-1"));

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }







}
