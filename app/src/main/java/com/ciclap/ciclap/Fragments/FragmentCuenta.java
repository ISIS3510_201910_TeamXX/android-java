package com.ciclap.ciclap.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ciclap.ciclap.Externals.GlideApp;
import com.ciclap.ciclap.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;


public class FragmentCuenta extends Fragment {


    private OnFragmentInteractionListener mListener;
    private TextView nombres,km,calorias,contaminacion;

    // Image View Account Image
    private CircleImageView accountProfilePhoto;


    public FragmentCuenta() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_cuenta, container, false);

        nombres =(TextView)view.findViewById(R.id.nombre);
        km = (TextView)view.findViewById(R.id.km);
        calorias = (TextView)view.findViewById(R.id.calorias);
        contaminacion = (TextView)view.findViewById(R.id.contaminadas);
        accountProfilePhoto = view.findViewById(R.id.imagePhotoAccountFr);

        datos();

        return view ;
    }


    public void datos(){

        // Firebase User
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("usuarios").child(currentUser.getUid());


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){

                    String url = "";

                        for(DataSnapshot ds: dataSnapshot.getChildren()){

                            nombres.setText(ds.child("nombre").getValue().toString());
                            km.setText(ds.child("kilometros").getValue().toString());
                            calorias.setText(ds.child("calorias").getValue().toString());
                            contaminacion.setText(ds.child("calidad_aire").getValue().toString());

                            if (ds.child("profilePictureURL").getValue() != null){
                                url = ds.child("profilePictureURL").getValue().toString();
                            }

                            else {
                                url = "https://previews.123rf.com/images/baldyrgan/baldyrgan1810/baldyrgan181000021/110200862-cycling-race-stylized-symbol-outlined-cyclist-vector-silhouette.jpg";
                            }
                        }
                    GlideApp.with(getContext()).load(url).into(accountProfilePhoto);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
