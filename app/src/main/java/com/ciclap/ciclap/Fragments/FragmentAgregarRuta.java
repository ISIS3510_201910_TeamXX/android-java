package com.ciclap.ciclap.Fragments;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ciclap.ciclap.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentAgregarRuta extends AppCompatActivity {

    private static final int AUTOCOMPLETE_INICIO = 1;
    private static final int AUTOCOMPLETE_DESTINO = 2;
    TextView origen, destino, nombre, descrp;
    CardView cardView;
    DatabaseReference dataBase;
    FirebaseAuth firebase;
    private LatLng ubicacionActual;
    private LatLng autoDestino;

    private void iniciarPlaces() {

        try {
            Places.initialize(getApplicationContext(), "AIzaSyCOQT4gfDJWMxg-XYRk413ROMF4I3TsZzE");
        } catch (Exception e) {

        }
    }
        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_agregar_ruta);

        iniciarPlaces();
        origen = findViewById(R.id.origen);
        destino = findViewById(R.id.destino);
        nombre = findViewById(R.id.nombreDeLaRuta);
        descrp = findViewById(R.id.descripcionRutaaaaaa);
        dataBase = FirebaseDatabase.getInstance().getReference();
        firebase = FirebaseAuth.getInstance();
        origen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



// Set the fields to specify which types of place data to
// return after the user has made a selection.
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

// Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields).setCountry("COL")
                        .build(getApplicationContext());

                startActivityForResult(intent, AUTOCOMPLETE_INICIO);

            }
        });
        destino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



// Set the fields to specify which types of place data to
// return after the user has made a selection.
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

// Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields).setCountry("COL")
                        .build(getApplicationContext());

                startActivityForResult(intent, AUTOCOMPLETE_DESTINO);

            }
        });

        cardView = findViewById(R.id.cardAgregarRuta);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String origenF = origen.getText().toString();
                String destinoF = destino.getText().toString();
                String nombreF = nombre.getText().toString();
                String descrpF = descrp.getText().toString();

                if (!origenF.isEmpty() && !destinoF.isEmpty() && !nombreF.isEmpty() && !descrpF.isEmpty()){

                    Map<String, Object> map = new HashMap<>();
                    map.put("nombre", origenF);
                    map.put("origen", autoDestino);
                    map.put("destino", ubicacionActual);
                    map.put("descripcion", descrpF);

                    String userID = firebase.getCurrentUser().getUid();
                    dataBase.child("usuarios").child(userID).child("user").child("mis_rutas").child(nombreF).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                startActivity(new Intent(FragmentAgregarRuta.this, RutaCreadaActivity.class));
                            }

                            else {
                                Toast.makeText(FragmentAgregarRuta.this, "No se pudo guardar la ruta.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

                else {
                    Toast.makeText(FragmentAgregarRuta.this, "Ninguno de los campos puede ser vacío.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_DESTINO) {
            if (resultCode == RESULT_OK) {


                Place place = Autocomplete.getPlaceFromIntent(data);
                autoDestino = place.getLatLng();
                destino.setText(place.getName());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == AUTOCOMPLETE_INICIO) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                ubicacionActual = place.getLatLng();
                origen.setText(place.getName());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                // The user canceled the operation.
            }
        }
    }


}
