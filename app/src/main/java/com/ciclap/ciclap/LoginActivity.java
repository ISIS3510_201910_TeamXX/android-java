package com.ciclap.ciclap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    // Regular Exp
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    // Attributes - Login
    EditText userField;
    EditText passwordField;
    CardView userLoginButton;

    TextView goToRegister;

    // Firebase
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //getActionBar().hide();

        userField = findViewById(R.id.userField);
        passwordField = findViewById(R.id.passwordField);
        userLoginButton = findViewById(R.id.cardLogin);
        goToRegister = findViewById(R.id.registerText);

        //validateEmail();
        firebaseAuth = FirebaseAuth.getInstance();

        goToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentReg = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intentReg);
            }
        });



        userLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!userField.getText().toString().isEmpty() && !passwordField.getText().toString().isEmpty()){

                    if(validateEmail()){

                        firebaseAuth.signInWithEmailAndPassword(userField.getText().toString(),
                                passwordField.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    startActivity(new Intent(LoginActivity.this, Home.class));
                                    finish();
                                }

                                else{
                                    Toast.makeText(LoginActivity.this, "Correo o contraseña inválida",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }

                else {
                    Toast.makeText(LoginActivity.this, "Ninguno de los campos puede ser vacío",
                            Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    //------------------
    // Validate E-mail
    //------------------

    private boolean validateEmail(){
        String userInput = userField.getText().toString().trim();

        if(userInput.isEmpty()){
            userField.setError("El correo no puede ser vacío");
            return false;
        }

        else if (!EMAIL_ADDRESS_PATTERN.matcher(userInput).matches()){
            userField.setError("Por favor ingresa un correo válido");
            return false;
        }

        else {return true;}
    }

}
