package com.ciclap.ciclap;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                home();

            }
        },2000);
    }

    private void home() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if(user != null){
            Intent intent = new Intent(this, Home.class);
            startActivity(intent);
            //moveTaskToBack(true);
            finish();
        }

        else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            //moveTaskToBack(true);
            finish();
        }
    }
}
