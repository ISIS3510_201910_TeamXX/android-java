package com.ciclap.ciclap;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.ciclap.ciclap.Entidades.Historial;
import com.ciclap.ciclap.Externals.GlideApp;
import com.ciclap.ciclap.Fragments.FragmentAyuda;
import com.ciclap.ciclap.Fragments.FragmentComunidad;
import com.ciclap.ciclap.Fragments.FragmentConfiguracion;
import com.ciclap.ciclap.Fragments.FragmentCuenta;
import com.ciclap.ciclap.Fragments.FragmentHistorial;
import com.ciclap.ciclap.Fragments.FragmentPrincipal;
import com.ciclap.ciclap.Fragments.FragmentRutas;
import com.ciclap.ciclap.Fragments.FragmentSignOut;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,FragmentConfiguracion.OnFragmentInteractionListener,FragmentRutas.OnFragmentInteractionListener,FragmentCuenta.OnFragmentInteractionListener, FragmentPrincipal.OnFragmentInteractionListener,FragmentHistorial.OnListFragmentInteractionListener{

    DrawerLayout drawer;
    Toolbar toolbar;

    // Para cambiar el nombre del usuario
    private TextView nombreUsuario;
    private CircleImageView panelPhoto;

    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        Fragment principal = new FragmentPrincipal();
        getSupportFragmentManager().beginTransaction().add(R.id.principal,principal).commit();

        // Get User-Name
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getUserName();
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {



        // Handle navigation view item clicks here.
        Fragment fragment =null;
        boolean fragmentseleccionado=false;
        switch (item.getItemId()) {
            case R.id.nav_cuenta:
                fragment = new FragmentCuenta();
                fragmentseleccionado=true;
                break;
            case  R.id.nav_rutas:
                fragment = new FragmentPrincipal();
                fragmentseleccionado=true;
                break;
            case R.id.nav_historial:
                fragment = new FragmentHistorial();
                fragmentseleccionado=true;
                break;

            case R.id.nav_comunidad:
                fragment = new FragmentComunidad();
                fragmentseleccionado=true;
                break;
            case R.id.nav_ayuda:
                fragment = new FragmentAyuda();
                fragmentseleccionado=true;
                break;
            case R.id.nav_log_out:
                fragment = new FragmentSignOut();
                fragmentseleccionado=true;
                break;
        }

        if (fragmentseleccionado) {
            getSupportFragmentManager().beginTransaction().replace(R.id.principal,fragment).commit();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onListFragmentInteraction(Historial item, int pos) {

    }

    // USERNAME - METHOD
    public void getUserName(){

        panelPhoto = findViewById(R.id.imagePanelUser);
        nombreUsuario = findViewById(R.id.usuarioPanel);

        if (nombreUsuario == null){
            Log.i("AHHH", "ES NULL");
        }

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("usuarios").child(currentUser.getUid());

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String url = "";
                String nombre = "";

                if(dataSnapshot.exists()){

                    for(DataSnapshot ds: dataSnapshot.getChildren()){
                        if( ds.child("nombre").getValue()!= null){
                            nombre = ds.child("nombre").getValue().toString();

                        }


                        if (ds.child("profilePictureURL").getValue() != null){
                            url = ds.child("profilePictureURL").getValue().toString();
                        }

                        else {
                             url = "https://previews.123rf.com/images/baldyrgan/baldyrgan1810/baldyrgan181000021/110200862-cycling-race-stylized-symbol-outlined-cyclist-vector-silhouette.jpg";
                        }
                    }
                }

                nombreUsuario.setText(nombre);
                GlideApp.with(getApplicationContext()).load(url).into(panelPhoto);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
